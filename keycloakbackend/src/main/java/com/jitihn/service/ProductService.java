package com.jitihn.service;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.jitihn.model.Product;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.result.DeleteResult;

import org.bson.Document;
import org.bson.types.ObjectId;

@ApplicationScoped
public class ProductService {

    @Inject
    MongoClient mongoClient;

    // list products
    public List<Product> get() {
        List<Product> products = new ArrayList<Product>();
        MongoCursor<Document> cursor = getCollection().find().iterator();

        try {
            while (cursor.hasNext()) {
                Document document = cursor.next();
                Product product = new Product();
                product.set_id(document.getObjectId("_id").toString());
                product.setName(document.getString("name"));
                product.setPrice(document.getDouble("price"));
                product.setDescription(document.getString("description"));
                products.add(product);
            }
        } finally {
            cursor.close();
        }
        return products;
    }

    // add product
    public void add(Product product) {
        Document document = new Document().append("name", product.getName()).append("price", product.getPrice())
                .append("description", product.getDescription());
        getCollection().insertOne(document);
    }

    public List<Product> delete(String _id) {
        Document document = new Document("_id", new ObjectId(_id));
        getCollection().deleteOne(document);
        return get();
    }

    private MongoCollection getCollection() {
        return mongoClient.getDatabase("microservicedb").getCollection("products");
    }

}