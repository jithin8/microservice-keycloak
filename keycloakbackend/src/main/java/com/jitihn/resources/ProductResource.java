package com.jitihn.resources;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.jitihn.model.Product;
import com.jitihn.service.ProductService;

@Path("/api/product")
public class ProductResource {

    @Inject
    ProductService productService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    public List<Product> get() {
        return productService.get();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({ "admin" })
    public List<Product> add(Product product) {
        productService.add(product);
        return get();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{_id}")
    @RolesAllowed({ "admin" })
    public List<Product> delete(@PathParam("_id") String _id) {
        System.out.println("delete id " + _id);
        return productService.delete(_id);
    }
}