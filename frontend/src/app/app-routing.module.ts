import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { ProfileComponent } from './components/profile/profile.component';
import { SettingsComponent } from './components/settings/settings.component';
import { EmployeeComponent } from './components/employee/employee.component';
import { ProfileEditComponent } from './components/profile/profile-edit/profile-edit.component';
import { CreateEmployeeComponent } from './components/employee/create-employee/create-employee.component';
import { ProductsComponent } from './components/products/products.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/profile',
    pathMatch: 'full'
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'profile_edit',
    component: ProfileEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'employees',
    component: EmployeeComponent,
    canActivate: [AuthGuard],
    data: {
      roles: ["admin"]
    }
  },
  {
    path: 'createemployee',
    component: CreateEmployeeComponent,
    canActivate: [AuthGuard],
    data: {
      roles: ["admin"]
    }
  },
  {
    path: 'products',
    component: ProductsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'settings',
    component: SettingsComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
