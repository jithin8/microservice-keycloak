import { Component, OnInit, SimpleChanges } from '@angular/core';
import { KeycloakProfile } from 'keycloak-js';
import { KeycloakService } from 'keycloak-angular';
import { AuthService } from 'src/app/services/auth.service';
import { NavService } from 'src/app/services/nav.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  title = 'frontend';
  // userDetails: KeycloakProfile;
  userDetails: any;
  username: string;
  isAdmin = false;
  isEmployee = false;
  constructor(private keycloakService: KeycloakService, private authService: AuthService, private navser: NavService, private dataService: DataService) { }

  async ngOnInit() {

    this.getuserinfo()

    this.navser.data.subscribe(async (data) => {
      this.getuserinfo()
      console.log(data);
    })

  }

  async getuserinfo() {
    // if (await this.keycloakService.isLoggedIn()) {
    //   this.userDetails = await this.keycloakService.loadUserProfile();
    //   this.isAdmin = this.authService.getRoles('admin');
    //   this.isEmployee = this.authService.getRoles('employee');
    //   console.log(this.userDetails);      
    // }
    if (await this.keycloakService.isLoggedIn()) {
      this.isAdmin = this.authService.getRoles('admin');
      this.isEmployee = this.authService.getRoles('employee');
      this.username = this.keycloakService.getUsername()
      this.dataService.getUserByUsername(this.username).subscribe(res => {
        this.userDetails = res[0];
      }
      );
    }
  }

  async doLogout() {
    await this.keycloakService.logout();
  }

}
