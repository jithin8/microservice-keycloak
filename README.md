# Keycloak sample application

## About 

Sample application that utilise Keycloak to provide Authentication and Authorization of user. 

User can Authenticate using Google Login, Custom Identity provider login and normal login/register mechanism.

